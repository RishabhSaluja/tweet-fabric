package com.example.rishabh.tweet;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Media;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.MediaService;
import com.twitter.sdk.android.core.services.StatusesService;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.mime.TypedFile;

public class Compose extends AppCompatActivity {
    EditText et1;
    TextView tvCharacters;
    Button btnPost, btnImage;
    ImageView iv1;
    TwitterSession twitterSession;

    final int PICK_FILE_REQ_CODE = 16;
    final int PERMISSION_READ_REQ_CODE = 17;

    File tweetMediaFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose);
        setUI();
    }

    private void setUI() {

        et1 = (EditText) findViewById(R.id.tweetbox);
        tvCharacters = (TextView) findViewById(R.id.textbox);
        btnPost = (Button) findViewById(R.id.tweet_button);
        btnImage = (Button) findViewById(R.id.photo_button);
        iv1 = (ImageView) findViewById(R.id.imageView);
        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tweetMediaFile != null) {
                    uploadImage(tweetMediaFile, et1.getText().toString());
                } else {
                    postTweet(et1.getText().toString(), null);
                }
                et1.setText(null);
            }
        });

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(Compose.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    openFilePicker();
                } else {
                    ActivityCompat.requestPermissions(Compose.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_READ_REQ_CODE);
                }
            }
        });


        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (charactersCountOk(s.toString())) {
                    btnPost.setEnabled(true);
                } else {
                    btnPost.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean charactersCountOk(String text) {
        int numerUrls = 0;
        int lengthAllUrls = 0;

        String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        Pattern urlPattern = Pattern.compile(regex);
        Matcher urlMatcher = urlPattern.matcher(text);
        while (urlMatcher.find()) {
            lengthAllUrls += urlMatcher.group().length();
            numerUrls++;
        }

        int tweetLength = text.length() - lengthAllUrls + numerUrls * 23;
        if (tweetMediaFile != null) {
            tweetLength += 24;
        }

        tvCharacters.setText(Integer.toString(140 - tweetLength));

        if (tweetLength > 0 && tweetLength <= 140) {
            return true;
        } else {
            return false;
        }
    }

    private void uploadImage(File imagefile, final String tweettext) {
        TypedFile typedFile = new TypedFile("image/*", imagefile);
        TwitterApiClient twitterApiClient = new TwitterApiClient(twitterSession);
        MediaService mediaservice = twitterApiClient.getMediaService();
        mediaservice.upload(typedFile, null, null, new Callback<Media>() {
            @Override
            public void success(Result<Media> result) {
                postTweet(tweettext, result.data.mediaIdString);
                tweetMediaFile = null;
                iv1.setImageDrawable(null);
            }

            @Override
            public void failure(TwitterException e) {
                Toast.makeText(Compose.this, "Medienupload fehlgeschlagen. Bitte erneut versuchen.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void postTweet(String text, String imageId) {
        StatusesService statusesService = Twitter.getApiClient().getStatusesService();
        statusesService.update(text, null, false, null, null, null, false, false, imageId, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                Toast.makeText(Compose.this, "Tweet gepostet.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(TwitterException e) {
                Toast.makeText(Compose.this, "Tweet konnte nicht gepostet werden.", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        });
    }

    private void openFilePicker() {
        Intent pickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pickerIntent.setType("image/*");
        startActivityForResult(pickerIntent, PICK_FILE_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_READ_REQ_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_REQ_CODE && resultCode == RESULT_OK) {
            Uri contentUri = data.getData();
            String filepath = DocumentHelper.getPath(Compose.this, contentUri);
            tweetMediaFile = new File(filepath);
            if (tweetMediaFile.length() > 5242880) {
                Toast.makeText(Compose.this, "Datei ist zu groß! Maximalgröße: 5MB", Toast.LENGTH_SHORT).show();
                tweetMediaFile = null;
            } else {
                iv1.setImageURI(contentUri);
                if (charactersCountOk(et1.getText().toString())) {
                    btnPost.setEnabled(true);
                } else {
                    btnPost.setEnabled(false);
                }
            }
        }
    }
}