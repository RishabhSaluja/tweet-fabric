package com.example.rishabh.tweet;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;
import com.twitter.sdk.android.tweetui.TweetViewAdapter;

import java.util.List;

public class HashTag extends ActionBarActivity {
    private boolean loading;
    private boolean search_end;
    private static String search_query = "@Humblebrag";
    private TweetViewAdapter adapter;
    private static final String search_result_type = "recent";
    private static final int search_count = 100;
    private long maxId;
    ListView list;
    TwitterSession twitterSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_hashtag);
        setProgressBarIndeterminateVisibility(true);
        handleintent(getIntent());
        adapter = new TweetViewAdapter(HashTag.this);
        list = (ListView) findViewById(R.id.listview1);
        list.setAdapter(adapter);
        list.setEmptyView(findViewById(R.id.loading));

        final SearchService service = Twitter.getApiClient().getSearchService();
        service.tweets(search_query, null, null, null, search_result_type, search_count, null, null, maxId, true, new Callback<Search>() {
            @Override
            public void success(Result<Search> result) {
                setProgressBarIndeterminateVisibility(true);
                final List<Tweet> tweets = result.data.tweets;
                adapter.getTweets().addAll(tweets);
                adapter.notifyDataSetChanged();

                if(tweets.size()>0){
                    maxId =tweets.get(tweets.size()-1).id-1;
                }else{
                    search_end = true;
                }
                loading = false;
            }

            @Override
            public void failure(TwitterException e) {
                setProgressBarIndeterminateVisibility(false);
                Toast.makeText(HashTag.this,"Failed to load!", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tag, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_search){
            return true;
        }
        if(id == R.id.menu_tweet){
            startActivity(new Intent(HashTag.this,Compose.class));
            finish();
        }
        if(id == R.id.menu_logout && twitterSession!=null){
            Twitter.logOut();
            startActivity(new Intent(HashTag.this,Login.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleintent(intent);
        super.onNewIntent(intent);
    }

    private void handleintent(Intent intent) {
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            search_query = intent.getStringExtra(SearchManager.QUERY);

            Log.d(search_query, "search");
        }
    }
}
/*
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TimelineResult;
import com.twitter.sdk.android.tweetui.internal.TimelineDelegate;

public class HashTag extends RecyclerView.Adapter<HashTag.TweetViewHolder>
        implements SwipeRefreshLayout.OnRefreshListener {

    private static final int ITEMS_PER_RESULT = 10;
    private final TimelineDelegate<Tweet> tweetsDelegate;
    private final TweetsCallback cb;
    private final Context context;
    @Nullable
    private OnRefreshFinishedListener refreshListener;

    public HashTag(Context ctx) {
        cb = new TweetsCallback();
        context = ctx;
        tweetsDelegate = new TimelineDelegate<>(new SearchTimeline.Builder()
                .query("@Humblebrag")
                .resultType(SearchTimeline.ResultType.RECENT)
                .maxItemsPerRequest(ITEMS_PER_RESULT)
                .build());
        tweetsDelegate.refresh(cb);
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Tweet tweet = tweetsDelegate.getItem(0);
        CompactTweetView tv = new CompactTweetView(context, tweet);
        return new TweetViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {
        Tweet tweet = tweetsDelegate.getItem(position);
        CompactTweetView tv = (CompactTweetView) holder.itemView;
        tv.setTweet(tweet);
    }

    @Override
    public int getItemCount() {
        return tweetsDelegate.getCount();
    }

    @Override
    public void onRefresh() {
        tweetsDelegate.refresh(cb);
    }

    public void setRefreshFinishedListener(@Nullable OnRefreshFinishedListener refreshListener) {
        this.refreshListener = refreshListener;
    }

    public interface OnRefreshFinishedListener {
        void onRefreshFinished();
    }

    static final class TweetViewHolder extends RecyclerView.ViewHolder {

        public TweetViewHolder(CompactTweetView itemView) {
            super(itemView);
        }
    }

    private final class TweetsCallback extends Callback<TimelineResult<Tweet>> {

        @Override
        public void success(Result<TimelineResult<Tweet>> result) {
            notifyDataSetChanged();
            notifyListener();
        }

        @Override
        public void failure(TwitterException exception) {
            notifyListener();
        }

        private void notifyListener() {
            if (refreshListener != null) {
                refreshListener.onRefreshFinished();
            }
        }
    }
}*/